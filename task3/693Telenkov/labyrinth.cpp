#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <SOIL2.h>

#include <iostream>
#include <vector>
#include <algorithm>

#define SHADOW_TEXTURE_UNIT  GL_TEXTURE1
#define SHADOW_TEXTURE_UNIT_IND  1
#define FREE_TEXTURE_UNIT  GL_TEXTURE2
#define FREE_TEXTURE_UNIT_IND  2

enum meshType { Floor, Wall, Ceiling, Poster1, Poster2 };

std::vector<std::pair<MeshPtr, meshType>> LabirinthBuilder(float length,
    float width,
    float height,
    const std::vector<std::vector<int>>& labirinth,
    std::vector<std::vector<bool>>& visited,
    int x,
    int y,
    glm::vec3 position) {
    assert(labirinth[y][x]);
    if (visited[y][x]) {
        return std::vector<std::pair<MeshPtr, meshType>>();
    } else {
        visited[y][x] = true;
    }

    auto x_size = labirinth[0].size(), y_size = labirinth.size();

    auto answer = std::vector<std::pair<MeshPtr, meshType>>();
    auto toPosition = glm::translate(glm::mat4(1.0f), position);

    auto mesh = makeRoomFloor(length, width, height, false);
    mesh->setModelMatrix(toPosition * mesh->modelMatrix());
    answer.emplace_back(mesh, Floor);

    mesh = makeRoomCeiling(length, width, height, false);
    mesh->setModelMatrix(toPosition * mesh->modelMatrix());
    answer.emplace_back(mesh, Ceiling);

    if ((x + 1 < x_size) && labirinth[y][x + 1]) {
        auto new_answ = LabirinthBuilder(
            length,
            width,
            height,
            labirinth,
            visited,
            x + 1,
            y,
            position + glm::vec3(length * 2, 0.0, 0.0));

        answer.resize(answer.size() + new_answ.size());
        std::copy(new_answ.begin(), new_answ.end(), answer.end() - new_answ.size());
    } else {
        if (labirinth[y][x] == 2 || labirinth[y][x] == 4) {
            mesh = makeRoomFrontWall(length, width, height, true);
            mesh->setModelMatrix(toPosition * mesh->modelMatrix());
            answer.emplace_back(mesh, Poster1);
        } else {
            mesh = makeRoomFrontWall(length, width, height, false);
            mesh->setModelMatrix(toPosition * mesh->modelMatrix());
            answer.emplace_back(mesh, Wall);
        }
    }

    if ((x - 1 >= 0) && labirinth[y][x - 1]) {
        auto new_answ = LabirinthBuilder(
            length,
            width,
            height,
            labirinth,
            visited,
            x - 1,
            y,
            position + glm::vec3(-length * 2, 0.0, 0.0));

        answer.resize(answer.size() + new_answ.size());
        std::copy(new_answ.begin(), new_answ.end(), answer.end() - new_answ.size());
    } else {
        if (labirinth[y][x] == 3 || labirinth[y][x] == 4) {
            mesh = makeRoomBackWall(length, width, height, true);
            mesh->setModelMatrix(toPosition * mesh->modelMatrix());
            answer.emplace_back(mesh, Poster2);
        } else {
            mesh = makeRoomBackWall(length, width, height, false);
            mesh->setModelMatrix(toPosition * mesh->modelMatrix());
            answer.emplace_back(mesh, Wall);
        }
    }

    if ((y + 1 < y_size) && labirinth[y + 1][x]) {
        auto new_answ = LabirinthBuilder(
            length,
            width,
            height,
            labirinth,
            visited,
            x,
            y + 1,
            position + glm::vec3(0.0, length * 2, 0.0));

        answer.resize(answer.size() + new_answ.size());
        std::copy(new_answ.begin(), new_answ.end(), answer.end() - new_answ.size());
    } else {
        mesh = makeRoomRightWall(length, width, height, false);
        mesh->setModelMatrix(toPosition * mesh->modelMatrix());
        answer.emplace_back(mesh, Wall);
    }

    if ((y - 1 >= 0) && labirinth[y - 1][x]) {
        auto new_answ = LabirinthBuilder(
            length,
            width,
            height,
            labirinth,
            visited,
            x,
            y - 1,
            position + glm::vec3(0.0, -length * 2, 0.0));

        answer.resize(answer.size() + new_answ.size());
        std::copy(new_answ.begin(), new_answ.end(), answer.end() - new_answ.size());
    } else {
        mesh = makeRoomLeftWall(length, width, height, false);
        mesh->setModelMatrix(toPosition * mesh->modelMatrix());
        answer.emplace_back(mesh, Wall);
    }

    return answer;
}

void invertY(unsigned char* image, int width, int height, int channels)
{
    for (int j = 0; j * 2 < height; ++j)
    {
        unsigned int index1 = j * width * channels;
        unsigned int index2 = (height - 1 - j) * width * channels;
        for (int i = 0; i < width * channels; i++)
        {
            unsigned char temp = image[index1];
            image[index1] = image[index2];
            image[index2] = temp;
            ++index1;
            ++index2;
        }
    }
}

void AddTexture(int number, GLuint& texture, const char* name) {
    glActiveTexture(FREE_TEXTURE_UNIT + number);
    // Выбираем номер текстуры
    glBindTexture(GL_TEXTURE_2D, texture);
    // Устанавливаем настройки фильтрации и преобразований (на текущей текстуре)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Загружаем текстуру
    int width, height, channels;
    auto image = SOIL_load_image(name, &width, &height, &channels, SOIL_LOAD_RGB);

    invertY(image, width, height, channels);
    // Привязываем картинку к текстуре
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);
}

class ShadowCubeMapFBO
{
public:
    ShadowCubeMapFBO();

    ~ShadowCubeMapFBO();

    bool Init(unsigned int WindowWidth, unsigned int WindowHeight);

    void BindForWriting(GLenum CubeFace);

    void BindForReading(GLenum TextureUnit);

private:
    GLuint m_fbo;
    GLuint m_shadowMap;
    GLuint m_depth;
};

ShadowCubeMapFBO::ShadowCubeMapFBO()
{
    m_fbo = 0;
    m_shadowMap = 0;
    m_depth = 0;
}

ShadowCubeMapFBO::~ShadowCubeMapFBO()
{
    if (m_fbo != 0) {
        glDeleteFramebuffers(1, &m_fbo);
    }

    if (m_shadowMap != 0) {
        glDeleteTextures(1, &m_shadowMap);
    }

    if (m_depth != 0) {
        glDeleteTextures(1, &m_depth);
    }
}

bool ShadowCubeMapFBO::Init(unsigned int WindowWidth, unsigned int WindowHeight)
{
    // Create the FBO
    glGenFramebuffers(1, &m_fbo);

    // Create the depth buffer
    glGenTextures(1, &m_depth);
    glBindTexture(GL_TEXTURE_2D, m_depth);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, WindowWidth, WindowHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);

    // Create the cube map
    glGenTextures(1, &m_shadowMap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_shadowMap);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    for (int i = 0; i < 6; i++) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_R32F, WindowWidth, WindowHeight, 0, GL_RED, GL_FLOAT, NULL);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depth, 0);

    // Disable writes to the color buffer
    glDrawBuffer(GL_NONE);

    // Disable reads from the color buffer
    glReadBuffer(GL_NONE);

    GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (Status != GL_FRAMEBUFFER_COMPLETE) {
        printf("FB error, status: 0x%x\n", Status);
        return false;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return true;
}

struct CameraDirection
{
    GLenum CubemapFace;
    glm::vec3 Target;
    glm::vec3 Up;
};

const int NUM_OF_LAYERS = 6;
CameraDirection gCameraDirections[NUM_OF_LAYERS] =
{
    { GL_TEXTURE_CUBE_MAP_POSITIVE_X, glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f) },
    { GL_TEXTURE_CUBE_MAP_NEGATIVE_X, glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f) },
    { GL_TEXTURE_CUBE_MAP_POSITIVE_Y, glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f) },
    { GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f) },
    { GL_TEXTURE_CUBE_MAP_POSITIVE_Z, glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f) },
    { GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f) }
};

void ShadowCubeMapFBO::BindForWriting(GLenum CubeFace)
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, CubeFace, m_shadowMap, 0);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
}

void ShadowCubeMapFBO::BindForReading(GLenum TextureUnit)
{
    glActiveTexture(TextureUnit);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_shadowMap);
}


class SampleApplication : public Application
{
public:
    double time = 0;
    double old_time;
    bool debug = false;

    std::vector<std::pair<MeshPtr, meshType>> _walls;
    std::vector<std::vector<int>> _labirinth;
    size_t begin_x, begin_y;

    MeshPtr _debug_cube;
    float cube_x = 0, cube_y = 0, cube_z = 0;

    bool trash = false;
    MeshPtr _trash_sphere1;
    MeshPtr _trash_sphere2;

    MeshPtr _marker; //Маркер для источника света

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;
    ShaderProgramPtr _shadowShader;
    ShaderProgramPtr _cubeShader;
    ShaderProgramPtr _textShader;

    bool outsideCamera = false;

    float length = 5.0f;
    float width = 1.0f;
    float height = 6.0f;

    std::shared_ptr<FPCameraMover> _cameraInside = std::make_shared<FPCameraMover>(height, height / 2);
    std::shared_ptr<OrbitCameraMover> _cameraOutside = std::make_shared<OrbitCameraMover>();

    // Параметры головного фонаря
    float _minAngle = 0.9;
    float _maxDiff = 0.2;
    //Координаты источника света
    float _lr = 50.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    //Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;

    //Карта теней
    ShadowCubeMapFBO m_shadowMapFBO;
    float near = 1.0f;
    float far = 50.0f;

    unsigned int depthCubemap;
    const unsigned int SHADOW_WIDTH = 2024, SHADOW_HEIGHT = 2024;

    //Параметры материала
    glm::vec3 _wallsAmbientColor;
    glm::vec3 _wallsDiffuseColor;
    glm::vec3 _wallsSpecularColor;

    // Текстуры
    GLuint texture[14];
    bool normal_map_on = true;

    void makeScene() override
    {
        Application::makeScene();

        //==============================================
        //Карта теней
        m_shadowMapFBO.Init(SHADOW_WIDTH, SHADOW_HEIGHT);
        //==============================================

        _debug_cube = makeCube(5.0);
        _debug_cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _trash_sphere1 = makeSphere(1.0);
        _trash_sphere1->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 5.0f, 4.5f)));

        _trash_sphere2 = makeSphere(1.0);
        _trash_sphere2->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, 0.0f, 3.5f)));
        old_time = glfwGetTime();

        //=========================================================
        //Инициализация шейдеров
        _shader = std::make_shared<ShaderProgram>("693TelenkovData3/labyrinth.vert", "693TelenkovData3/labyrinth.frag");
        _markerShader = std::make_shared<ShaderProgram>("693TelenkovData3/marker.vert", "693TelenkovData3/marker.frag");
        _shadowShader = std::make_shared<ShaderProgram>("693TelenkovData3/shadow.vert", "693TelenkovData3/shadow.frag");
        _cubeShader = std::make_shared<ShaderProgram>("693TelenkovData3/skybox.vert", "693TelenkovData3/skybox.frag");
        _textShader = std::make_shared<ShaderProgram>("693TelenkovData3/text.vert", "693TelenkovData3/text.frag");

        //=========================================================

        _marker = makeSphere(0.1f);

        _labirinth = {
            {1, 1, 1, 1, 1},
            {4, 0, 1, 0, 2},
            {1, 1, 1, 1, 1},
            {1, 0, 1, 0, 4},
            {1, 1, 1, 1, 1},
        };

        std::vector<std::vector<bool>> visited = std::vector<std::vector<bool>>(
            _labirinth.size(),
            std::vector<bool>(_labirinth[0].size(), false));

        begin_y = 2;
        begin_x = 2;
        //Создание и загрузка мешей
        _walls = LabirinthBuilder(length, width, height, _labirinth, visited, begin_x, begin_y, glm::vec3(0.0, 0.0, 0.0));

        _cameraInside->setNearFarPlanes(0.1, 100.0f);
        _cameraOutside->setNearFarPlanes(0.1, 1000.0f);

        //Инициализация значений переменных освщения
        _lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
        _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);

        //Инициализация материала лабиринта
        _wallsAmbientColor = glm::vec3(1.0, 1.0, 1.0);
        _wallsDiffuseColor = glm::vec3(1.0, 1.0, 1.0);
        _wallsSpecularColor = glm::vec3(1.0, 1.0, 1.0);

        //Текстуры

        glGenTextures(14, texture);
        AddTexture(0, texture[0], "693TelenkovData3/Images/Stone_Wall_014_basecolor.jpg");
        AddTexture(1, texture[1], "693TelenkovData3/Images/Stone_Wall_014_normal.jpg");
        AddTexture(2, texture[2], "693TelenkovData3/Images/Wood_012_Tiles_basecolor.jpg");
        AddTexture(3, texture[3], "693TelenkovData3/Images/Wood_012_Tiles_normal.jpg");
        AddTexture(4, texture[4], "693TelenkovData3/Images/Wood_Ceiling_Coffers_001_basecolor.jpg");
        AddTexture(5, texture[5], "693TelenkovData3/Images/Wood_Ceiling_Coffers_001_normal.jpg");
        AddTexture(6, texture[6], "693TelenkovData3/Images/animedonttouch.jpg");
        AddTexture(7, texture[7], "693TelenkovData3/Images/animedonttouch1.jpg");
        AddTexture(8, texture[8], "693TelenkovData3/Images/animedonttouch2.jpg");
        AddTexture(9, texture[9], "693TelenkovData3/Images/animedonttouch3.jpg");
        AddTexture(10, texture[10], "693TelenkovData3/Images/animedonttouch4.jpg");
        AddTexture(11, texture[11], "693TelenkovData3/Images/omaewa.jpg");
        AddTexture(12, texture[12], "693TelenkovData3/Images/omaewa1.jpg");
        AddTexture(13, texture[13], "693TelenkovData3/Images/omaewa2.jpg");
        AddTexture(14, texture[0], "693TelenkovData3/Images/Stone_Wall_014_basecolor.jpg");

    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Labirinth", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::Checkbox("Outside camera", &outsideCamera);
            ImGui::Checkbox("Ray Tracing", &normal_map_on);
            ImGui::Checkbox("Debug", &debug);
            ImGui::Checkbox("Trash", &trash);


            if (ImGui::CollapsingHeader("Light color"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_lightAmbientColor));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_lightDiffuseColor));
            }

            if (ImGui::CollapsingHeader("Light pointed"))
            {
                ImGui::SliderFloat("radius", &_lr, 0.1f, 100.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }

            if (ImGui::CollapsingHeader("Light head"))
            {
                ImGui::SliderFloat("MinAngle", &_minAngle, 0.0f, 1.0f);
                ImGui::SliderFloat("MaxDiff", &_maxDiff, 0.0f, 1.0f);
            }

            if (ImGui::CollapsingHeader("Cube"))
            {
                ImGui::SliderFloat("X", &cube_x, -50.0f, 50.0f);
                ImGui::SliderFloat("Y", &cube_y, -50.0f, 50.0f);
                ImGui::SliderFloat("Z", &cube_z, -50.0f, 50.0f);
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        _shadowShader->use();
        glClearColor(FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX);

        float aspect = (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT;
        glm::vec3 lightPos;
        glm::vec3 lightDir;

        lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
        lightPos = lightDir * _lr;

        _shadowShader->setFloatUniform("far_plane", far);
        _shadowShader->setVec3Uniform("lightPos", lightPos);

        glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, near, far);

        for (int i = 0; i < NUM_OF_LAYERS; i++) {
            m_shadowMapFBO.BindForWriting(gCameraDirections[i].CubemapFace);
            glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
            glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

            _shadowShader->setMat4Uniform("camera", shadowProj *
                glm::lookAt(lightPos, lightPos + gCameraDirections[i].Target, gCameraDirections[i].Up));

            if (/*debug*/false) {
                _shadowShader->setMat4Uniform("model", _debug_cube->modelMatrix());
                _debug_cube->draw();
            } else {
                for (auto meshTypePair : _walls) {
                    _shadowShader->setMat4Uniform("model", meshTypePair.first->modelMatrix());
                    meshTypePair.first->draw();
                }
            }
            if (trash) {
                _shadowShader->setMat4Uniform("model", _trash_sphere1->modelMatrix());
                _trash_sphere1->draw();
                _shadowShader->setMat4Uniform("model", _trash_sphere2->modelMatrix());
                _trash_sphere2->draw();
            }
        }


        //Рисуем сцену с тенями
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int widthc, heightc;
        glfwGetFramebufferSize(_window, &widthc, &heightc);

        glViewport(0, 0, widthc, heightc);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер		
        _shader->use();

        if (outsideCamera) {
            _cameraMover = _cameraOutside;
            _shader->setVec3Uniform("cameraPos", _cameraOutside->getPosition());
            _shader->setIntUniform("useHeadLight", 0);
        } else {
            _cameraMover = _cameraInside;
            _shader->setIntUniform("useHeadLight", 1);
            _shader->setVec3Uniform("cameraPos", _cameraInside->get_position());
        }

        lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
        lightPos = lightDir * _lr;

        _shader->setVec3Uniform("mainLight.pos", lightPos);
        _shader->setVec3Uniform("mainLight.dir", lightDir);
        _shader->setFloatUniform("mainLight.minAngle", 1);
        _shader->setFloatUniform("mainLight.farPlane", far);
        _shader->setVec3Uniform("mainLight.Ld", _lightDiffuseColor);

        lightDir = _cameraInside->get_direction();
        lightPos = _cameraInside->get_position();

        _shader->setVec3Uniform("headLight.pos", lightPos);
        _shader->setVec3Uniform("headLight.dir", lightDir);
        _shader->setFloatUniform("headLight.minAngle", _minAngle);
        _shader->setFloatUniform("headLight.maxDiff", _maxDiff);
        _shader->setVec3Uniform("headLight.Ld", _lightDiffuseColor);


        _shader->setVec3Uniform("ambientColor", _lightAmbientColor);

        //Подключаем нужную камеру
        if (outsideCamera) {
            _cameraMover = _cameraOutside;
            _shader->setVec3Uniform("cameraPos", _cameraOutside->getPosition());
            _shader->setIntUniform("useHeadLight", 0);
        } else {
            _cameraMover = _cameraInside;
            _shader->setIntUniform("useHeadLight", 1);
            _shader->setVec3Uniform("cameraPos", _cameraInside->get_position());
        }
        _camera = _cameraMover->cameraInfo();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        if (debug) {
            _cubeShader->use();
            m_shadowMapFBO.BindForReading(SHADOW_TEXTURE_UNIT);
            _cubeShader->setTextureUniform("cubeTex", SHADOW_TEXTURE_UNIT_IND);
            _cubeShader->setMat4Uniform("modelMatrix", glm::translate(_debug_cube->modelMatrix(), {cube_x, cube_y, cube_z}));
            _cubeShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
            _cubeShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
            _debug_cube->draw();
            _shader->use();
        }

        m_shadowMapFBO.BindForReading(SHADOW_TEXTURE_UNIT);
        _shader->setTextureUniform("mainDepthMap", SHADOW_TEXTURE_UNIT_IND);
        // Таймер
        auto cur_time = glfwGetTime();
        time += cur_time - old_time;
        old_time = cur_time;

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        int i = 0;
        int firstPosterFrame = int(time);
        int secondPosterFrame = int(time);

        for (auto meshTypePair : _walls) {
            _shader->setMat4Uniform("modelMatrix", meshTypePair.first->modelMatrix());

            _shader->setVec3Uniform("material.Ka", glm::vec3(_wallsAmbientColor));
            _shader->setVec3Uniform("material.Kd", glm::vec3(_wallsDiffuseColor));
            _shader->setVec3Uniform("material.Ks", glm::vec3(_wallsSpecularColor));

            switch (meshTypePair.second)
            {
            case Floor:
                _shader->setTextureUniform("samplerTexture", FREE_TEXTURE_UNIT_IND + 2);
                _shader->setTextureUniform("samplerNormal", FREE_TEXTURE_UNIT_IND + 3);
                _shader->setIntUniform("useSamplerNormal", normal_map_on);
                break;
            case Wall:
                _shader->setTextureUniform("samplerTexture", FREE_TEXTURE_UNIT_IND + 0);
                _shader->setTextureUniform("samplerNormal", FREE_TEXTURE_UNIT_IND + 1);
                _shader->setIntUniform("useSamplerNormal", normal_map_on);
                break;
            case Ceiling:
                _shader->setTextureUniform("samplerTexture", FREE_TEXTURE_UNIT_IND + 4);
                _shader->setTextureUniform("samplerNormal", FREE_TEXTURE_UNIT_IND + 5);
                _shader->setIntUniform("useSamplerNormal", normal_map_on);
                break;
            case Poster1:
                _shader->setTextureUniform("samplerTexture", FREE_TEXTURE_UNIT_IND + 6 + firstPosterFrame % 5);
                _shader->setIntUniform("useSamplerNormal", 0);
                break;
            case Poster2:
                _shader->setTextureUniform("samplerTexture", FREE_TEXTURE_UNIT_IND + 11 + firstPosterFrame % 3);
                _shader->setIntUniform("useSamplerNormal", 0);
                break;
            }
            meshTypePair.first->draw();
        }

        if (trash) {
            _shader->setTextureUniform("samplerTexture", FREE_TEXTURE_UNIT_IND + 0);
            _shader->setIntUniform("useSamplerNormal", 0);
            _shader->setMat4Uniform("modelMatrix", _trash_sphere1->modelMatrix());
            _trash_sphere1->draw();
            _shader->setMat4Uniform("modelMatrix", _trash_sphere2->modelMatrix());
            _trash_sphere2->draw();
        }
        

        //Рисуем маркер для источника света		
        _markerShader->use();
        lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
        lightPos = lightDir * _lr;
        _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), lightPos));
        _markerShader->setVec4Uniform("color", glm::vec4(_lightDiffuseColor, 1.0f));
        _marker->draw();

    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}