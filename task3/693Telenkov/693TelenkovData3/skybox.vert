#version 330

uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

uniform mat4 modelMatrix; //матрица для превращения координат из локальной системы кординат в текстурные координаты

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат

out vec3 texCoord; //текстурные координаты

void main()
{
	texCoord = (vec4(vertexPosition, 1.0)).xyz;
	
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
