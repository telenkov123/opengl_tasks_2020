#version 330

uniform sampler2D cubeTex;

in vec2 texCoord; //���������� ���������� (��������������� ����� ��������� ������������)

out vec4 fragColor; //�������� ���� ���������

void main()
{
	vec3 texColor = texture(cubeTex, texCoord).rgb;

	fragColor = vec4(texColor, 1.0);
}
