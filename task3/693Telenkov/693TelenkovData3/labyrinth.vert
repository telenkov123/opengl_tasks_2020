
#version 330

//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты



layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины
layout(location = 3) in vec3 vertexTangent; //касательная в локальной системе координат
layout(location = 4) in vec3 vertexBiTangent; //бинормаль в локальной системе координат

out vec3 normalWorldSpace;
out vec3 posWorldSpace;
out vec2 texCoords;
out vec3 tangentWorldSpace;
out vec3 bitangentWorldSpace;

void main()
{
	normalWorldSpace = (modelMatrix * vec4(vertexNormal, 0.0)).xyz; //нормаль в мировую

	tangentWorldSpace = (modelMatrix * vec4(vertexTangent, 0.0)).xyz; //касательная в мировую

	bitangentWorldSpace = (modelMatrix * vec4(vertexBiTangent, 0.0)).xyz; //бикасательная в мировую
	
    posWorldSpace = (modelMatrix * vec4(vertexPosition, 1.0)).xyz; //координаты вершины из локальной в мировую
    
	texCoords = vertexTexCoord;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
