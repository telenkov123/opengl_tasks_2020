#version 330 core
layout (location = 0) in vec3 aPos;

uniform mat4 model;
uniform mat4 camera;

out vec3 WorldPos;
                  
void main()
{
    vec4 Pos4 = vec4(aPos, 1.0);
    vec4 WorldPos4 = model * Pos4;    
    gl_Position = camera * WorldPos4;
    WorldPos = WorldPos4.xyz;    
}
