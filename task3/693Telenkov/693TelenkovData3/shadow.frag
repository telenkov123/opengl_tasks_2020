#version 330 core
in vec3 WorldPos;

uniform vec3 lightPos;
uniform float far_plane;

out float fragColor; //�������� ���� ���������

void main()
{
    // ���������� ���������� ����� ���������� � ���������� 
    float lightDistance = length(WorldPos - lightPos);
    
    // �������������� � ��������� [0, 1] ����������� ������� �� far_plane
    lightDistance = lightDistance / far_plane;
    
	fragColor = lightDistance;
    // ������ ���������� � �������������� ������� ���������
}  