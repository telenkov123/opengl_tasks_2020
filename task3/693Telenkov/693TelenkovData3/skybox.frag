#version 330

uniform samplerCube cubeTex;

in vec3 texCoord; //текстурные координаты (интерполированы между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	float texColor = texture(cubeTex, texCoord).r;

	fragColor = vec4(vec3(1, 1, 1) * texColor, 1.0);
}
