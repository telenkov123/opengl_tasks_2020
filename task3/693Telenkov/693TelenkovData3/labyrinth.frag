
#version 330

struct LightInfo
{
    vec3 dir; //направление источника света в мировой системе координат (для направленного источника)
    vec3 pos; //положение источника света в мировой системе координат (для точечного источника)
	float minAngle; //минимальный угол фонаря (1, если не фонарь)
	float maxDiff; //максимальный угол фонаря (1, если не фонарь)
	float farPlane;
    vec3 Ld; //цвет и интенсивность света
};
uniform LightInfo mainLight;
uniform samplerCube mainDepthMap;
uniform vec3 ambientColor;

uniform int useHeadLight;
uniform LightInfo headLight;

uniform vec3 cameraPos;

vec3 Specular(LightInfo light, vec3 frag_normal, vec3 frag_pos, vec3 view_pos) {
    vec3 lightDirCamSpace = normalize(light.pos - frag_pos); //направление от фрагмента к источнику
    vec3 viewDir = normalize(view_pos - frag_pos);
    vec3 reflectDir = reflect(-lightDirCamSpace, frag_normal);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDirCamSpace + viewDir);  
    spec = pow(max(dot(frag_normal, halfwayDir), 0.0), 64.0);
    return spec * light.Ld;    
}

vec3 LightColor(LightInfo light, vec3 frag_normal, vec3 frag_pos) {
    vec3 lightDirCamSpace = normalize(light.pos - frag_pos); //направление от фрагмента к источнику
	
    float NdotL = max(dot(frag_normal.xyz, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
	float RadiusDegradation = 1;
	if (light.minAngle < 0.999) {
		float LightAngle = dot(light.dir.xyz, -lightDirCamSpace.xyz);
		if (LightAngle < light.minAngle) {
			RadiusDegradation = max( (light.maxDiff - (light.minAngle - LightAngle)) / light.maxDiff, 0);
		} else {
			RadiusDegradation = 1;
		}
	}

	return light.Ld * RadiusDegradation * NdotL;
}

float ShadowCalculation(samplerCube depth_map, vec3 frag_pos, vec3 light_pos, float light_far_plane)
{
    // расчет вектора между положением фрагмента и положением источника света
    vec3 fragToLight = frag_pos - light_pos;
    // полученный вектор направления от источника к фрагменту 
    // используется для выборки из кубической карты глубин
    float closestDepth = texture(depth_map, fragToLight).r;
    // получено линейное значение глубины в диапазоне [0,1]
    // проведем обратную трансформацию в исходный диапазон
    closestDepth *= light_far_plane;
    // получим линейное значение глубины для текущего фрагмента 
    // как расстояние от фрагмента до источника света
    float currentDepth = length(fragToLight);
    // тест затенения
    float bias = 0.01; 
    float shadow = currentDepth -  bias > closestDepth ? 1.0 : 0.0;

    return shadow;
}  

uniform sampler2D samplerTexture;
uniform int useSamplerNormal;
uniform sampler2D samplerNormal;

struct MaterialInfo
{
    vec3 Ka; //коэффициент отражения окружающего света
    vec3 Kd; //коэффициент отражения диффузного света
    vec3 Ks; //коэффициент отражения specular света
};
uniform MaterialInfo material;

in vec3 normalWorldSpace;
in vec3 posWorldSpace;
in vec2 texCoords;
in vec3 tangentWorldSpace;
in vec3 bitangentWorldSpace;

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	vec3 normal;
	if (useSamplerNormal == 1) {
		vec3 normalCoefs = texture(samplerNormal, texCoords).xyz;
		normalCoefs = normalize(normalCoefs * 2.0 - 1.0);   
		normal = normalCoefs.b * normalWorldSpace + normalCoefs.r * tangentWorldSpace + normalCoefs.g * bitangentWorldSpace;
	} else {
		normal = normalWorldSpace;
	}
	normal = normalize(normal);

	vec3 color_diffuse;
	vec3 color_specular;
    
	if (useHeadLight > 0) {
		color_diffuse += LightColor(headLight, normal, posWorldSpace);
		color_specular += Specular(headLight, normal, posWorldSpace, cameraPos);
	}
    
	if (ShadowCalculation(mainDepthMap, posWorldSpace, mainLight.pos, mainLight.farPlane) < 0.95) {
		color_diffuse += LightColor(mainLight, normal, posWorldSpace);
		color_specular += Specular(mainLight, normal, posWorldSpace, cameraPos);
	}

    vec3 color = texture(samplerTexture, texCoords).rgb;
	color *= ambientColor * material.Ka + color_diffuse * material.Kd + color_specular * material.Ks;
	
	//vec3 color = vec3(1, 1, 1);
    fragColor = vec4(color, 1.0); //просто копируем
}
