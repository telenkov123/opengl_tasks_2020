#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <SOIL2.h>

#include <iostream>
#include <vector>
#include <algorithm>

enum meshType {Floor, Wall, Ceiling, Poster1, Poster2};

std::vector<std::pair<MeshPtr, meshType>> LabirinthBuilder(float length,
                                      float width,
                                      float height,
                                      const std::vector<std::vector<int>>& labirinth,
                                      std::vector<std::vector<bool>>& visited,
                                      int x,
                                      int y,
                                      glm::vec3 position) {
    assert(labirinth[y][x]);
    if (visited[y][x]) {
        return std::vector<std::pair<MeshPtr, meshType>>();
    } else {
        visited[y][x] = true;
    }

    auto x_size = labirinth[0].size(), y_size = labirinth.size();

    auto answer = std::vector<std::pair<MeshPtr, meshType>>();
    auto toPosition = glm::translate(glm::mat4(1.0f), position);

    auto mesh = makeRoomFloor(length, width, height, false);
    mesh->setModelMatrix(toPosition * mesh->modelMatrix());
    answer.emplace_back(mesh, Floor);

    mesh = makeRoomCeiling(length, width, height, false);
    mesh->setModelMatrix(toPosition * mesh->modelMatrix());
    answer.emplace_back(mesh, Ceiling);

    if ((x + 1 < x_size) && labirinth[y][x + 1]) {
        auto new_answ = LabirinthBuilder(
            length,
            width,
            height,
            labirinth,
            visited,
            x + 1,
            y,
            position + glm::vec3(length * 2, 0.0, 0.0));

        answer.resize(answer.size() + new_answ.size());
        std::copy(new_answ.begin(), new_answ.end(), answer.end() - new_answ.size());
    } else {
        if (labirinth[y][x] == 2 || labirinth[y][x] == 4) {
            mesh = makeRoomFrontWall(length, width, height, true);
            mesh->setModelMatrix(toPosition * mesh->modelMatrix());
            answer.emplace_back(mesh, Poster1);
        } else {
            mesh = makeRoomFrontWall(length, width, height, false);
            mesh->setModelMatrix(toPosition * mesh->modelMatrix());
            answer.emplace_back(mesh, Wall);
        }
    }

    if ((x - 1 >= 0) && labirinth[y][x - 1]) {
        auto new_answ = LabirinthBuilder(
            length,
            width,
            height,
            labirinth,
            visited,
            x - 1,
            y,
            position + glm::vec3(-length * 2, 0.0, 0.0));

        answer.resize(answer.size() + new_answ.size());
        std::copy(new_answ.begin(), new_answ.end(), answer.end() - new_answ.size());
    } else {
        if (labirinth[y][x] == 3 || labirinth[y][x                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ] == 4) {
            mesh = makeRoomBackWall(length, width, height, true);
            mesh->setModelMatrix(toPosition * mesh->modelMatrix());
            answer.emplace_back(mesh, Poster2);
        } else {
            mesh = makeRoomBackWall(length, width, height, false);
            mesh->setModelMatrix(toPosition * mesh->modelMatrix());
            answer.emplace_back(mesh, Wall);
        }
    }

    if ((y + 1 < y_size) && labirinth[y + 1][x]) {
        auto new_answ = LabirinthBuilder(
            length,
            width,
            height,
            labirinth,
            visited,
            x,
            y + 1,
            position + glm::vec3(0.0, length * 2, 0.0));

        answer.resize(answer.size() + new_answ.size());
        std::copy(new_answ.begin(), new_answ.end(), answer.end() - new_answ.size());
    } else {
        mesh = makeRoomRightWall(length, width, height, false);
        mesh->setModelMatrix(toPosition * mesh->modelMatrix());
        answer.emplace_back(mesh, Wall);
    }

    if ((y - 1 >= 0) && labirinth[y - 1][x]) {
        auto new_answ = LabirinthBuilder(
            length,
            width,
            height,
            labirinth,
            visited,
            x,
            y - 1,
            position + glm::vec3(0.0, -length * 2, 0.0));

        answer.resize(answer.size() + new_answ.size());
        std::copy(new_answ.begin(), new_answ.end(), answer.end() - new_answ.size());
    } else {
        mesh = makeRoomLeftWall(length, width, height, false);
        mesh->setModelMatrix(toPosition * mesh->modelMatrix());
        answer.emplace_back(mesh, Wall);
    }

    return answer;
}

void invertY(unsigned char* image, int width, int height, int channels)
{
    for (int j = 0; j * 2 < height; ++j)
    {
        unsigned int index1 = j * width * channels;
        unsigned int index2 = (height - 1 - j) * width * channels;
        for (int i = 0; i < width * channels; i++)
        {
            unsigned char temp = image[index1];
            image[index1] = image[index2];
            image[index2] = temp;
            ++index1;
            ++index2;
        }
    }
}

void AddTexture(int number, GLuint& texture, const char* name) {
    // Выбираем номер текстуры
    glActiveTexture(GL_TEXTURE0 + number);
    glBindTexture(GL_TEXTURE_2D, texture);
    // Устанавливаем настройки фильтрации и преобразований (на текущей текстуре)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Загружаем текстуру
    int width, height, channels;
    auto image = SOIL_load_image(name, &width, &height, &channels, SOIL_LOAD_RGB);

    invertY(image, width, height, channels);
    // Привязываем картинку к текстуре
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);
}

class SampleApplication : public Application
{
public:
    double time = 0;
    double old_time;
    bool debug = false;

    std::vector<std::pair<MeshPtr, meshType>> _walls;
    std::vector<std::vector<int>> _labirinth;
    size_t begin_x, begin_y;

    MeshPtr _debug_cube;

    MeshPtr _marker; //Маркер для источника света

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    bool outsideCamera = false;

    float length = 5.0f;
    float width = 1.0f;
    float height = 6.0f;

    std::shared_ptr<FPCameraMover> _cameraInside = std::make_shared<FPCameraMover>(height, height / 2);
    std::shared_ptr<OrbitCameraMover> _cameraOutside = std::make_shared<OrbitCameraMover>();

    // Параметры головного фонаря
    float _minAngle = 0.9;
    float _maxDiff = 0.2;
    //Координаты источника света
    float _lr = 50.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    //Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;

    //Параметры материала
    glm::vec3 _wallsAmbientColor;
    glm::vec3 _wallsDiffuseColor;
     
    // Текстуры
    GLuint texture[14];
    unsigned char* wall_tex, *floor_tex, *ceiling_tex;
    bool normal_map_on = true;

    void makeScene() override
    {
        Application::makeScene();

        _debug_cube = makeCube(5.0);
        _debug_cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        old_time = glfwGetTime();

        //=========================================================
        //Инициализация шейдеров
        _shader = std::make_shared<ShaderProgram>("693TelenkovData1/labyrinth.vert", "693TelenkovData1/labyrinth.frag");
        _markerShader = std::make_shared<ShaderProgram>("693TelenkovData1/marker.vert", "693TelenkovData1/marker.frag");

        //=========================================================

        _marker = makeSphere(0.1f);

        _labirinth = {
            {1, 1, 1, 1, 1},
            {4, 0, 1, 0, 2},
            {1, 1, 1, 1, 1},
            {1, 0, 1, 0, 4},
            {1, 1, 1, 1, 1},
        };

        std::vector<std::vector<bool>> visited = std::vector<std::vector<bool>>(
            _labirinth.size(),
            std::vector<bool>(_labirinth[0].size(), false));

        begin_y = 2;
        begin_x = 2;
        //Создание и загрузка мешей
        _walls = LabirinthBuilder(length, width, height, _labirinth, visited, begin_x, begin_y, glm::vec3(0.0, 0.0, 0.0));

        _cameraInside->setNearFarPlanes(0.1, 100.0f);
        _cameraOutside->setNearFarPlanes(0.1, 1000.0f);

        //Инициализация значений переменных освщения
        _lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
        _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);

        //Инициализация материала лабиринта
        _wallsAmbientColor = glm::vec3(1.0, 1.0, 1.0);
        _wallsDiffuseColor = glm::vec3(1.0, 1.0, 1.0);

        //Текстуры

        glGenTextures(14, texture);
        AddTexture(0, texture[0], "693TelenkovData1/Images/Stone_Wall_014_basecolor.jpg");
        AddTexture(1, texture[1], "693TelenkovData1/Images/Stone_Wall_014_normal.jpg");
        AddTexture(2, texture[2], "693TelenkovData1/Images/Wood_012_Tiles_basecolor.jpg");
        AddTexture(3, texture[3], "693TelenkovData1/Images/Wood_012_Tiles_normal.jpg");
        AddTexture(4, texture[4], "693TelenkovData1/Images/Wood_Ceiling_Coffers_001_basecolor.jpg");
        AddTexture(5, texture[5], "693TelenkovData1/Images/Wood_Ceiling_Coffers_001_normal.jpg");
        AddTexture(6, texture[6], "693TelenkovData1/Images/animedonttouch.jpg");
        AddTexture(7, texture[7], "693TelenkovData1/Images/animedonttouch1.jpg");
        AddTexture(8, texture[8], "693TelenkovData1/Images/animedonttouch2.jpg");
        AddTexture(9, texture[9], "693TelenkovData1/Images/animedonttouch3.jpg");
        AddTexture(10, texture[10], "693TelenkovData1/Images/animedonttouch4.jpg");
        AddTexture(11, texture[11], "693TelenkovData1/Images/omaewa.jpg");
        AddTexture(12, texture[12], "693TelenkovData1/Images/omaewa1.jpg");
        AddTexture(13, texture[13], "693TelenkovData1/Images/omaewa2.jpg");
        AddTexture(0, texture[0], "693TelenkovData1/Images/Stone_Wall_014_basecolor.jpg");
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Labirinth", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::Checkbox("Outside camera", &outsideCamera);
            ImGui::Checkbox("Ray Tracing", &normal_map_on);
            ImGui::Checkbox("Debug", &debug);


            if (ImGui::CollapsingHeader("Light color"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_lightAmbientColor));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_lightDiffuseColor));
            }

            if (ImGui::CollapsingHeader("Light outside"))
            {
                ImGui::SliderFloat("radius", &_lr, 0.1f, 100.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }

            if (ImGui::CollapsingHeader("Light head"))
            {
                ImGui::SliderFloat("MinAngle", &_minAngle, 0.0f, 1.0f);
                ImGui::SliderFloat("MaxDiff", &_maxDiff, 0.0f, 1.0f);
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        //Подключаем шейдер		
        _shader->use();

        glm::vec3 lightPos;
        glm::vec3 lightDir;

        //Подключаем нужную камеру
        if (outsideCamera) {
            lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
            lightPos = lightDir * _lr;

            _shader->setFloatUniform("light.minAngle", 1);

            _cameraMover = _cameraOutside;
        } else {
            lightDir = _cameraInside->get_direction();
            lightPos = _cameraInside->get_position();

            _shader->setFloatUniform("light.minAngle", _minAngle);
            _shader->setFloatUniform("light.maxDiff", _maxDiff);

            _cameraMover = _cameraInside;
        }
        _camera = _cameraMover->cameraInfo();

        glm::vec4 lightPosCamSpace = _camera.viewMatrix * glm::vec4(lightPos, 1.0);
        glm::vec4 lightDirCamSpace = _camera.viewMatrix * glm::vec4(lightDir, 0.0);

        _shader->setVec3Uniform("light.pos", glm::vec3(lightPosCamSpace));
        _shader->setVec3Uniform("light.dir", glm::vec3(lightDirCamSpace));

        
        _shader->setVec3Uniform("light.La", _lightAmbientColor);
        _shader->setVec3Uniform("light.Ld", _lightDiffuseColor);

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        if (debug) {
            _shader->setTextureUniform("samplerTexture", 0);
            _shader->setIntUniform("useSamplerNormal", 0);

            _shader->setMat4Uniform("modelMatrix", _debug_cube->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _debug_cube->modelMatrix()))));
            _shader->setVec3Uniform("material.Ka", glm::vec3(_wallsAmbientColor));
            _shader->setVec3Uniform("material.Kd", glm::vec3(_wallsDiffuseColor));
            _debug_cube->draw();
        }
        else {
            // Таймер
            auto cur_time = glfwGetTime();
            time += cur_time - old_time;
            old_time = cur_time;

            //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
            int i = 0;
            int firstPosterFrame = int(time);
            int secondPosterFrame = int(time);

            for (auto meshTypePair : _walls) {
                _shader->setMat4Uniform("modelMatrix", meshTypePair.first->modelMatrix());
                _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * meshTypePair.first->modelMatrix()))));

                _shader->setVec3Uniform("material.Ka", glm::vec3(_wallsAmbientColor));
                _shader->setVec3Uniform("material.Kd", glm::vec3(_wallsDiffuseColor));

                switch (meshTypePair.second)
                {
                case Floor:
                    _shader->setTextureUniform("samplerTexture", 2);
                    _shader->setTextureUniform("samplerNormal", 3);
                    _shader->setIntUniform("useSamplerNormal", normal_map_on);
                    break;
                case Wall:
                    _shader->setTextureUniform("samplerTexture", 0);
                    _shader->setTextureUniform("samplerNormal", 1);
                    _shader->setIntUniform("useSamplerNormal", normal_map_on);
                    break;
                case Ceiling:
                    _shader->setTextureUniform("samplerTexture", 4);
                    _shader->setTextureUniform("samplerNormal", 5);
                    _shader->setIntUniform("useSamplerNormal", normal_map_on);
                    break;
                case Poster1:
                    _shader->setTextureUniform("samplerTexture", 6 + firstPosterFrame % 5);
                    _shader->setIntUniform("useSamplerNormal", 0);
                    break;
                case Poster2:
                    _shader->setTextureUniform("samplerTexture", 11 + firstPosterFrame % 3);
                    _shader->setIntUniform("useSamplerNormal", 0);
                    break;
                }
                meshTypePair.first->draw();
            }
        }
        
        //Рисуем маркер для источника света		
        if (outsideCamera) {
            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), lightPos));
            _markerShader->setVec4Uniform("color", glm::vec4(_lightDiffuseColor, 1.0f));
            _marker->draw();
        }

    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}