
#version 330

struct LightInfo
{
    vec3 dir; //направление на источник света в камерной системе координат (для направленного источника)
    vec3 pos; //положение источника света в камерной системе координат (для точечного источника)
	float minAngle; //минимальный угол фонаря (1, если не фонарь)
	float maxDiff; //максимальный угол фонаря (1, если не фонарь)
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
};
uniform LightInfo light;

uniform sampler2D samplerTexture;
uniform int useSamplerNormal;
uniform sampler2D samplerNormal;

struct MaterialInfo
{
    vec3 Ka; //коэффициент отражения окружающего света
    vec3 Kd; //коэффициент отражения диффузного света
};
uniform MaterialInfo material;

in vec3 normalCamSpace;
in vec3 posCamSpace;
in vec2 texCoords;
in vec3 tangentCamSpace;
in vec3 bitangentCamSpace;

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	vec3 normal;
	if (useSamplerNormal == 1) {
		vec3 normalCoefs = texture(samplerNormal, texCoords).xyz;
		normalCoefs = normalize(normalCoefs * 2.0 - 1.0);   
		normal = normalCoefs.x * normalCamSpace + normalCoefs.y * tangentCamSpace + normalCoefs.z * bitangentCamSpace;
	} else {
		normal = normalCamSpace;
	}

    vec3 lightDirCamSpace = normalize(light.pos - posCamSpace); //направление от фрагмента к источнику
	
    float NdotL = max(dot(normal.xyz, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
	float RadiusDegradation = 1;
	if (light.minAngle < 0.999) {
		float LightAngle = dot(light.dir.xyz, -lightDirCamSpace.xyz);
		if (LightAngle < light.minAngle) {
			RadiusDegradation = max( (light.maxDiff - (light.minAngle - LightAngle)) / light.maxDiff, 0);
		} else {
			RadiusDegradation = 1;
		}
	}

	//vec3 color = normal;
    vec3 color = texture(samplerTexture, texCoords).rgb;
	color *= light.La * material.Ka + light.Ld * material.Kd * RadiusDegradation * NdotL;

    fragColor = vec4(color, 1.0); //просто копируем
}
